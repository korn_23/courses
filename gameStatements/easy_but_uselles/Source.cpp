#include<SFML/Graphics.hpp>
#include "MenuGameState.h"
#include "MainGameState.h"

extern void mainCallback(int value);

GameState *activeGameState;

int main() {
	sf::RenderWindow *window = new sf::RenderWindow(sf::VideoMode(800, 600), "Test");

	activeGameState = new MenuGameState();
	activeGameState->init();


	while (window->isOpen()) {
		sf::Event e;
		while (window->PollEvent(e)) {
			if (e.type == sf::Event::Closed) {
				window->close();
			}

			activeGameState->handleEvent(e);
		}
		
		activeGameState->update(0); 
		 
		window->clear();
		activeGameState->draw(window);
		window->display();
		int result = activeGameState->getResult();
		if (result == 1) {
			delete activeGameState
				activeGameState = new MainGameState();
			activeGameState->init();
		}
		else if(result==2 ){
			delete activeGameState;
			activeGameState = new MenuGameState();
			activeGameState->init();
		}
	}

	return 0;
}