#pragma once
#include"GameState.h"

class MainGameState :public GameState {
private:
	sf::RectangleShape = *rect;
	int result = 0;
public:
	~MainGameState() {
		delete rect;
	}

	void init() {
		rect = new sf::RectangleShape();
		rect->setPosition(sf::Vector2f(0, 0));
		rect->setSize(sf::Vector2f(800, 600));
		rect->setFillColor(sf::Color::Yellow);
	}

	void handleEvent(sf::Event& e) {
		if (e.type == sf::Event::MouseButtonReleased) {
			if (e.mouseButton.button == sf::Mouse::Button::Right) {
				result = 2;
			}
		}
	}

	void update(float dt) {

	}

	void draw(sf::RenderWindow *window) {
		window->draw(*rect);
	}

	virtual int getResult() {
		return result;
	}
};