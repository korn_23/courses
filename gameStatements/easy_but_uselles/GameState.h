#pragma once
#include <SFML/Graphics.hpp>

class GameState {
public : 
	virtual void init() = 0;
	virtual void handleEvent(sf::Event& e) = 0;
	virtual void update(float dt) = 0;
	virtual void draw(sf::RenderWindow *window) = 0;
	virtual void getResult() = 0;
};